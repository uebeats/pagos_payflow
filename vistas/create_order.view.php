<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="uebeats">
    <title>Contratar Servicio | IntegramosWeb</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/c9d71cc307.js" crossorigin="anonymous"></script>
    <!-- Favicons -->
    <link rel="icon" href="https://integramosweb.pro/wp-content/uploads/2020/04/cropped-perfil-face-red-32x32.png" sizes="32x32">
    <link rel="icon" href="https://integramosweb.pro/wp-content/uploads/2020/04/cropped-perfil-face-red-192x192.png" sizes="192x192">
    <link rel="apple-touch-icon" href="https://integramosweb.pro/wp-content/uploads/2020/04/cropped-perfil-face-red-180x180.png">
    <meta name="theme-color" content="#563d7c">

    <!-- Custom styles for this template -->
    <link href="assets/css/maestro.css" rel="stylesheet">
</head>

<body>
    <div class="border-inweb"></div>
    <?php if (empty($valida['name_product_web'])) : ?>
        <div class="container">
            <div class="row">
                <div class="col"><?php echo $alert_e; ?></div>
            </div>
            <div class="row">
                <div class="col text-center">
                    <a href="validate_order.php?services=wc_webpay_tbk" class="btn btn-warning mt-3"><i class="fas fa-undo-alt"></i> Volver a intentarlo</a>
                </div>
            </div>
        </div>
    <?php else : ?>
        <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
            <span class="text-right text-uppercase"><small>Nº de Orden: <?php echo $n_order;?></small></span>
            <h1>Contratación de <?php echo $valida['name_product_web']; ?> </h1>
            <p class="lead">Bienvenido a la plataforma para contratación de servicios.</p>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-10 offset-lg-1">
                    <form id=formWebpay method="POST" action="payFlow/create.php">
                        <input type="hidden" id="n_order" name="n_order" value="<?php echo $n_order;?>">
                        <div class="form-row">
                            <div class="form-group col">
                                <label>ID de comercio <i class="fas fa-question-circle question-i text-marca" data-toggle="tooltip" data-placement="top" title="ID entregado por Transbank"></i></label>
                                <input name="id_comercio" id="id_comercio" type="text" class="form-control" placeholder="Ej: 20000540">
                            </div>
                            <div class="form-group col">
                                <label>Rut del comercio <i class="fas fa-question-circle question-i text-marca" data-toggle="tooltip" data-placement="top" title="Rut asociado al comercio"></i></label>
                                <input name="rut_comercio" type="text" class="form-control rut" placeholder="Ej: 76.111.111-1">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <label>URL del comercio <i class="fas fa-question-circle question-i text-marca" data-toggle="tooltip" data-placement="top" title="URL tienda online a integrar"></i></label>
                                <input name="url_website" id="url_website" type="text" class="form-control" placeholder="Ej: https://mitiendaonline.cl" required>
                            </div>
                            <div class="form-group col">
                                <label>Selecciona la plataforma <i class="fas fa-question-circle question-i text-marca" data-toggle="tooltip" data-placement="top" title="Plataforma de la tienda"></i></label>
                                <select name="select_ecommerce" id="select_ecommerce" class="form-control">
                                    <option value="woocommerce">Woocommerce</option>
                                    <option value="prestashop">Prestashop</option>
                                    <option value="opencart">Opencart</option>
                                    <option value="virtuemart">Virtuemart</option>
                                    <option value="magento">Magento</option>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <h3 class="font-weight-bold pb-2">Contacto administrativo o del desarrollador</h3>
                        <div class="form-row">
                            <div class="form-group col">
                                <label>Nombre Completo</label>
                                <input name="name_client" id="name_client" type="text" class="form-control" placeholder="Ej: Jesús Caballero Peralta" require>
                            </div>
                            <div class="form-group col">
                                <label>Correo Electrónico</label>
                                <input name="email_client" id="email_client" type="email" class="form-control" placeholder="Ej: correo@micorreo.cl" required>
                            </div>
                            <div class="form-group col">
                                <label>Teléfono Contacto</label>
                                <input name="phone_client" id="phone_client" type="tel" class="form-control" placeholder="Ej: +569 48992070" required>
                            </div>
                        </div>
                        <p>Luego de realizada la <span class="text-marca">contratación del servicio</span>, serán solicitadas las
                            <span class="text-marca">credenciales de acceso</span> a su plataforma, para realizar la integración de <span class="text-marca">Webpay Plus</span>, el plazo promedio
                            para la ejecución de la integración son <span class="text-marca">5 días hábiles</span>.</p>
                        <ul class="list-group mb-4">
                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div class="custom-control custom-radio">
                                    <input id="paymentMethod" name="paymentMethod" type="radio" class="custom-control-input" checked="" required="">
                                    <label class="custom-control-label" for="credit">Pagar usando 
                                        <img src="https://www.flow.cl/images/header/logo-flow.svg" alt="logo webpay" width="100">
                                    </label>
                                </div>
                                <span class="text-muted">$<?php echo number_format($valida['price_product_web'], 0, '', '.');?></span>
                                <input type="hidden" name="amount_trx" id="amount_trx" value="<?php echo $valida['price_product_web'];?>">
                            </li>
                        </ul>
                        <div class="form-row">
                            <div class="form-group col-sm-8 offset-sm-2 text-center">
                                <button type="submit" class="btn btn-danger btn-block font-weight-light">Contratar servicio <i class="text-white fas fa-check-circle"></i></button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    <?php endif;?>

    <div class="container">
        <footer class="pt-4 my-md-5 pt-md-5 border-top">
            <div class="row">
                <div class="col-12 col-md">
                    <a href="https://integramosweb.pro"><img class="mb-2" src="https://integramosweb.pro/wp-content/uploads/2020/04/logo2020-red.png" alt="logo integramosweb" width="40"></a>
		            <span class="text-center mr-5 mt-3" id="footer-info">© <?php echo date('Y'); ?> IntegramosWeb | Hecho con amor por <a href="https://www.facebook.com/unicoescritorbeats">@uebeats</a> para Chile y el mundo</span>

                </div>
                
            </div>
        </footer>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/@popperjs/core@2"></script>
    <script src="assets/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="assets/js/jquery.rut.js" crossorigin="anonymous"></script>

    <script>
        $(document).ready(function(){
            //formWebpay();
        })

        $(function() {
            $('.rut').rut();
            $('[data-toggle="tooltip"]').tooltip()
        })

        // var formWebpay = function(){
        //     $('#formWebpay').submit(function(e){
        //         e.preventDefault();
        //         var datos = $(this).serialize();
        //         // console.log(datos);

        //         $.ajax({

        //         })
        //     })
        // }
    </script>

</body>

</html>