<?php
/**
 * Clase para Configurar el cliente
 * @Filename: Config.class.php
 * @version: 2.0
 * @Author: flow.cl
 * @Email: csepulveda@tuxpan.com
 * @Date: 28-04-2017 11:32
 * @Last Modified by: Carlos Sepulveda
 * @Last Modified time: 28-04-2017 11:32
 */
 
 $COMMERCE_CONFIG = array(
 	"APIKEY" => "23FD9548-C707-4E8B-97DC-57711LFC14ED", // Registre aquí su apiKey
 	"SECRETKEY" => "4799344a08bed3ca3aa0a5b1a6f75f76bbc587b3", // Registre aquí su secretKey
	 //"APIURL" => "https://sandbox.flow.cl/api", // Producción EndPoint o Sandbox EndPoint
	"APIURL" => "https://www.flow.cl/api",
 	"BASEURL" => "https://pagos.integramosweb.pro" //Registre aquí la URL base en su página donde instalará el cliente
 );
 
 class Config {
 	
	static function get($name) {
		global $COMMERCE_CONFIG;
		if(!isset($COMMERCE_CONFIG[$name])) {
			throw new Exception("The configuration element thas not exist", 1);
		}
		return $COMMERCE_CONFIG[$name];
	}
 }