<?php

/**
 * Recibo variables de formulario
 */
$n_order = $_POST['n_order'];

$id_comercio = $_POST['id_comercio'];
$rut_comercio = $_POST['rut_comercio'];
$url_comercio = $_POST['url_website'];
$select_ecommerce = $_POST['select_ecommerce'];

$name_client = $_POST['name_client'];
$email_client = $_POST['email_client'];
$phone_client = $_POST['phone_client'];

$amount_trx = $_POST['amount_trx'];

/**
 * Validar si esta vacia la variable n_order
 * si esta vacia redirecionar a pagos
 */
if (empty($n_order)) {
	header('Location: https://pagos.integramosweb.pro/validate_order.php?services=wc_webpay_tbk');
} else {
	/**
	 * Ejemplo de creación de una orden de cobro, iniciando una transacción de pago
	 * Utiliza el método payment/create
	 */
	require(__DIR__ . "/lib/FlowApi.class.php");

	//Para datos opcionales campo "optional" prepara un arreglo JSON
	$optional = array(
		"Nombre Contacto" => $name_client,
		"Correo Contacto" => $email_client,
		"Telefono Contacto" => $phone_client
	);
	$optional = json_encode($optional);

	//Prepara el arreglo de datos
	$params = array(
		"commerceOrder" => $n_order,
		"subject" => "Pago por Integración",
		"currency" => "CLP",
		"amount" => $amount_trx,
		"email" => $email_client,
		"paymentMethod" => 9,
		"urlConfirmation" => Config::get("BASEURL") . "/payFlow/confirm.php",
		"urlReturn" => Config::get("BASEURL") . "/payFlow/result.php",
		"optional" => $optional
	);
	//Define el metodo a usar
	$serviceName = "payment/create";

	try {
		// Instancia la clase FlowApi
		$flowApi = new FlowApi;
		// Ejecuta el servicio
		$response = $flowApi->send($serviceName, $params, "POST");
		//Prepara url para redireccionar el browser del pagador
		$redirect = $response["url"] . "?token=" . $response["token"];
		header("location:$redirect");
	} catch (Exception $e) {
		echo $e->getCode() . " - " . $e->getMessage();
	}
}
