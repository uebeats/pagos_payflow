<?php

/**
 * Pagina del comercio para redireccion del pagador
 * A esta página Flow redirecciona al pagador pasando vía POST
 * el token de la transacción. En esta página el comercio puede
 * mostrar su propio comprobante de pago
 */
require(__DIR__ . "/lib/FlowApi.class.php");

try {
	//Recibe el token enviado por Flow
	if (!isset($_POST["token"])) {
		throw new Exception("No se recibio el token", 1);
	}
	$token = filter_input(INPUT_POST, 'token');
	$params = array(
		"token" => $token
	);
	//Indica el servicio a utilizar
	$serviceName = "payment/getStatus";
	$flowApi = new FlowApi();
	$response = $flowApi->send($serviceName, $params, "GET");
} catch (Exception $e) {
	echo "Error: " . $e->getCode() . " - " . $e->getMessage();
}
/**
 * Formateo de fecha
 */
$source = $response['paymentData']['date'];
$date = new DateTime($source);
?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="uebeats">
	<title>Contratar Servicio | IntegramosWeb</title>

	<!-- Bootstrap core CSS -->
	<link href="../assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/c9d71cc307.js" crossorigin="anonymous"></script>
	<!-- Favicons -->
	<link rel="icon" href="https://integramosweb.pro/wp-content/uploads/2020/04/cropped-perfil-face-red-32x32.png" sizes="32x32">
	<link rel="icon" href="https://integramosweb.pro/wp-content/uploads/2020/04/cropped-perfil-face-red-192x192.png" sizes="192x192">
	<link rel="apple-touch-icon" href="https://integramosweb.pro/wp-content/uploads/2020/04/cropped-perfil-face-red-180x180.png">
	<meta name="theme-color" content="#563d7c">

	<!-- Custom styles for this template -->
	<link href="../assets/css/maestro.css" rel="stylesheet">
	<style>
		body {
			background-color: #f4f4f4 !important;
		}

		.recibo-container {
			background-color: #fff;
			border: 1px solid #eaeaea;
		}

		.logo {
			max-height: 54%;
		}
	</style>
</head>
<?php
/**
 * El estado de la order
 * 1 pendiente de pago
 * 2 pagada
 * 3 rechazada
 * 4 anulada
 */

?>

<body>
	<div class="border-inweb"></div>
	<div class="container">
		<div class="row">
			<div class="mt-3 pt-3 col-lg-8 offset-lg-2">
				<h1 class="text-center"><a href="https://integramosweb.pro"><img class="logo" src="https://integramosweb.pro/wp-content/uploads/2020/04/logo2020-red.png" height="54px"></a></h1>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-8 offset-lg-2 recibo-container">
				<div class="mx-3 my-3">
					<h1 class="text-center">Comprobante de Pago</h1>
					<?php if($response['status'] == '2') :?>
					<div class="alert alert-success">
						<p class="mb-0 text-center"><strong>¡Perfecto!</strong> El pago fue recibido satisfactoriamente. Muchas gracias <i class="fas fa-handshake"></i></p>
					</div>
					<p>Estimado <strong><?php echo $response['optional']['Nombre Contacto']; ?></strong>, con fecha <strong><?php echo $date->format('d-m-Y'); ?></strong> hemos recibido
						el pago de su pedido <strong>Nº<?php echo $response['commerceOrder']; ?></strong>, el cual corresponde al servicio de <strong><?php echo $response['subject']; ?></strong>.<br><br>
						A continuación detalles de la transacción:</p>
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td scope="row">Medio de pago utilizado</td>
								<td><?php echo $response['paymentData']['media']; ?></td>
							</tr>
							<tr>
								<td scope="row">Monto de la transacción</td>
								<td>$<?php echo number_format($response['amount'], 0, '', '.'); ?></td>
							</tr>
							<tr>
								<td scope="row">Email del pagador</td>
								<td><?php echo $response['payer']; ?></td>
							</tr>
							<tr>
								<td>Detalles adicionales del pedido:</td>
								<td>
									<ul>
										<li><?php echo $response['optional']['Nombre Contacto'] ?></li>
										<li><?php echo $response['optional']['Correo Contacto'] ?></li>
										<li><?php echo $response['optional']['Telefono Contacto'] ?></li>
									</ul>
								</td>
							</tr>
						</tbody>
					</table>
					<?php endif;?>
					<?php if($response['status'] == '1' OR $response['status'] == '3' OR $response['status']== '4'):?>
					<div class="alert alert-danger">
						<p class="mb-0 text-center"><strong>¡Error!</strong> Por favor, vuelva a intentarlo más tarde.</p>
					</div>
					<p>Si el problema persiste, porfavor comunicate con el área de soporte en este <a href="mailto:web@integramosweb.pro">enlace</a></p>
					<?php endif;?>
				</div>
				<!-- fin success voucher @uebeats -->
			</div>
		</div>
		<div class="text-center mt-3" id="footer-info">© 2020 IntegramosWeb | Hecho con amor por <a href="https://www.facebook.com/unicoescritorbeats">@uebeats</a> para Chile y el mundo</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
	<script src="https://unpkg.com/@popperjs/core@2"></script>
	<script src="../assets/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
	<script src="../assets/js/jquery.rut.js" crossorigin="anonymous"></script>
</body>

</html>