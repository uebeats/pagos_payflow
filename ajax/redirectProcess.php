<?php
    /**
     * Solicita archivo de configuración
     */
    require '../config.php';

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        $services = filter_var($_POST['code_product'], FILTER_SANITIZE_STRING);
        $statement = $con->prepare('
        SELECT * FROM product_integramos WHERE code_product = :code_product'
        );
        $statement->execute(array(
            ':code_product' => $services
        ));
        $dataProduct = $statement->fetch(PDO::FETCH_ASSOC);

        if($dataProduct == true){
            echo 'ok';
        }else{
            echo 'error';
        }
    }