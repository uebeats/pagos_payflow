<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="uebeats">
    <title>Validando orden de compra - IntegramosWeb</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- <script src="https://kit.fontawesome.com/c9d71cc307.js" crossorigin="anonymous"></script> -->
    <!-- Favicons -->
    <link rel="icon" href="#" sizes="32x32" type="image/png">
    <meta name="theme-color" content="#563d7c">

    <!-- Custom styles for this template -->
    <link href="assets/css/maestro.css" rel="stylesheet">
</head>

<body onload="newOrderSystem()">
    <div class="container">
        <form id="formVerify" method="POST">
            <input id="code_product" type="hidden" value="<?php echo $_GET['services']; ?>">
            <input id="token" type="hidden" value="<?php $rand = rand(1, 99999); $token = hash('sha256', $rand); echo $token; ?>">
        </form>
        <div class="row mt-5">
            <div class="col alertId">
                <div class="alert alert-info">
                    <div id="delayMsg"></div>
                </div>
            </div>

        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/@popperjs/core@2"></script>
    <script src="assets/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script>
        function delayRedirect() {
            // Obtenemos el valor por el id
            var code_product = document.getElementById("code_product").value;
            var token = document.getElementById("token").value;
            document.getElementById('delayMsg').innerHTML = 'Redireccionando a contratación en <span id="countDown"> 5 </span> segundos....';
            var count = 5;
            setInterval(function() {
                count--;
                document.getElementById('countDown').innerHTML = count;
                if (count == 0) {
                    window.location = 'create_order.php?services=' + code_product + '&token=' + token +'';
                }
            }, 1000);
        }
        var newOrderSystem = function() {

            // Obtenemos el valor por el id
            var code_product = document.getElementById("code_product").value;

            $.ajax({
                type: "POST",
                url: "ajax/redirectProcess.php",
                data: {code_product: code_product},
                beforeSend: function() {
                    document.getElementById('delayMsg').innerHTML = 'Comprobando...';
                },
                success: function(data) {
                    if (data == 'ok') {
                        delayRedirect();
                    } else if (data == 'error') {
                        console.log(data);
                    } else {
                        console.log(data);
                    }
                }
            });
        }
    </script>
</body>

</html>