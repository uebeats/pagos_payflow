<?php
   /**
    * Define las constantes de conexion a la base de datos
    */
    define('DB_HOST', 'localhost');//DB_HOST:  generalmente suele ser "127.0.0.1"
    define('DB_USER', 'root');//Usuario de tu base de datos
    define('DB_PASS', '');//Contraseña del usuario de la base de datos
    define('DB_NAME', 'integram_pay');//Nombre de la base de datos

   global $con;
    try {
        $con = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8', DB_USER, DB_PASS);
      } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
      }

      $selProduct = $con->prepare("SELECT * FROM product_integramos");
      $selProduct->execute();
      $data = $selProduct->fetchAll(PDO::FETCH_ASSOC);
