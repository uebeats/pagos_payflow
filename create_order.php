<?php
    /**
     * Solicita archivo de configuración
     */
        require 'config.php';
    
    /**
     * Obtiene el valor GET de services
     */
        $code_product = $_GET['services'];
        $token = $_GET['token'];
        
        $alert_e = '';

        $statement = $con->prepare('
        SELECT * FROM product_integramos WHERE code_product = :services'
        );
        $statement->execute(array(
        ':services' => $code_product
        ));
        $valida = $statement->fetch();

        if($valida == false){
            $alert_e = '<div class="mt-3 alert alert-danger" role="alert">Hay un problema con la contratación de este servicio. Vuelve a intentarlo o comunicate con <a href="mailto:web@integramosweb.pro">soporte</a>. Error: 001</div>';
        }
    /**
     * Crear numero de orden
     */

     $n_order = rand(11111,99999);

    include 'vistas/create_order.view.php';
?>